package edu.graduate.recommend.cf

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.mllib.recommendation.MatrixFactorizationModel
import com.github.fommil.netlib.BLAS.{getInstance => blas}

/**
  * run script
  * cd /usr/local/spark/bin/
  * ./spark-submit --jars ~/lib/jedis.jar --class "edu.graduate.recommend.cf.Recommend" ~/workplace/graduate/recommender/target/recommender-1.0-SNAPSHOT.jar
  */
object Recommend{
  def main(args: Array[String]) : Unit = {
    val conf = new SparkConf().setAppName("Recommender").setMaster("local")
    val sc = new SparkContext(conf)
    val modelpath = "file:///home/xu/myCollaborativeFilter"
    val model = MatrixFactorizationModel.load(sc, modelpath)
    for(uid <- Array(1,2,3,4)){
      val rec = model.recommendProducts(uid, 5)
      val result = rec.map(x => x.product.toString)
      CacheUtil.persist(uid,result)
    }
    sc.stop()
  }

/**
  * 余弦相似度
  * T(x,y) = ∑x(i)y(i) / sqrt(∑(x(i)*x(i)) * ∑(y(i)*y(i)))
  */
  def cosineSimilarity(user_1:Int,user_2:Int,model:MatrixFactorizationModel): Double ={
    val feature_1 = model.userFeatures.lookup(user_1).head
    val feature_2 = model.userFeatures.lookup(user_2).head
    val length = feature_1.length
    val x = blas.ddot(length,feature_1,1,feature_2,2)
    val s_1 = blas.ddot(length,feature_1,1,feature_1,2)
    val s_2 = blas.ddot(length,feature_2,1,feature_2,2)
    x / Math.sqrt(s_1 * s_2)
  }
}

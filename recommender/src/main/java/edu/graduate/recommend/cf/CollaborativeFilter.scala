package edu.graduate.recommend.cf

import edu.graduate.recommend.ml.DBUtil
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.mllib.recommendation.{ALS, Rating}
import scala.collection.JavaConverters._

object CollaborativeFilter {
	def main(args: Array[String]): Unit = {
		val conf = new SparkConf().setAppName("CollaborativeFilter").setMaster("local")
		val sc = new SparkContext(conf)
		val data:scala.collection.mutable.Seq[Rating] = DBUtil.getAll().asScala
		val ratings = sc.parallelize(data).map(tuple => {
			Rating(tuple.user,tuple.product,tuple.rating)
		})
		val model = ALS.train(ratings,10,5,0.01)
		val users = ratings.map(x => x.user).distinct()
		for(u <- users){
			val result = model.recommendProducts(u,2).map(x => x.product.toString)
			CacheUtil.persist(u,result)
		}
		sc.stop()
	}
}

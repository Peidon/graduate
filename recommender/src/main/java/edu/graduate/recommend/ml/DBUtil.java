package edu.graduate.recommend.ml;


import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import org.apache.spark.mllib.recommendation.Rating;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DBUtil {
    public static void main(String[] args){
        getAll();
    }

    private static Connection getConn() {
        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/film";
        String username = "root";
        String password = "1234";
        Connection conn = null;
        try {
            Class.forName(driver); //classLoader,加载对应驱动
            conn = (Connection)DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    public static ArrayList<Rating> getAll(){
        Connection conn = getConn();
        String sql = "select user_id,item_id,value from rating";
        PreparedStatement pstmt;
        ArrayList<Rating> list = new ArrayList<>();
        Rating rating = null;
        try {
            pstmt = (PreparedStatement)conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                rating = new Rating(rs.getInt(1),rs.getInt(2),rs.getDouble(3));
                list.add(rating);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}

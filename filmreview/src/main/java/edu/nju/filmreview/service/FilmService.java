package edu.nju.filmreview.service;

import edu.nju.filmreview.common.page.PageData;
import edu.nju.filmreview.common.page.PageParams;
import edu.nju.filmreview.common.utils.CacheUtil;
import edu.nju.filmreview.interceptor.UserContext;
import edu.nju.filmreview.mapper.FilmMapper;
import edu.nju.filmreview.mapper.RatingMapper;
import edu.nju.filmreview.model.Film;
import edu.nju.filmreview.model.Rating;
import edu.nju.filmreview.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import java.util.List;

@Service
public class FilmService {

    @Autowired
    FilmMapper filmMapper;

    @Autowired
    RatingMapper ratingMapper;

    public List<Film> queryList(Film query, PageParams pageParams) {
        return filmMapper.list(query, pageParams);
    }

    public PageData<Film> queryFilm(Film query, PageParams pageParams) {
        List<Film> films = filmMapper.list(query, pageParams);
        Long count = filmMapper.selectPageCount(query);
        return PageData.buildPage(films, count, pageParams.getPageSize(), pageParams.getPageNum());
    }

    /**
     * 详情
     */
    public Film detailFilm(Integer filmId) {
        Film film = filmMapper.getOne(filmId);
        Jedis jedis = CacheUtil.jPool.getResource();
        final String fk = "film" + film.getId();
        if(jedis.hexists("avg",fk)){
            String avg = jedis.hget("avg",fk);
            film.setRating(Double.parseDouble(avg));
        }
        jedis.close();
        return film;
    }

    /**
     * 评分
     */
    public void updateRating(Double rating, final Integer id) {
        User user = UserContext.getUser();
        final Long uid = user.getId();
        final String fk = "film" + id;
        Jedis jedis = CacheUtil.jPool.getResource();
        if (!jedis.hexists(fk, uid + "")) {
            jedis.hincrBy("length", fk, 1);
        }
        jedis.hset(fk, uid.toString(), rating.toString());
        List<String> list = jedis.hvals(fk);
        Double sum = list.stream().map(Double::parseDouble).reduce(0.0, (a, b) -> a + b);
        Double avg = sum / Integer.parseInt(jedis.hget("length",fk));
        jedis.hset("avg",fk,avg.toString());
        Rating r = ratingMapper.getRating(uid, id);
        if (r == null) {
            ratingMapper.insert(uid, id, rating);
        } else {
            ratingMapper.update(uid, id, rating);
        }
        jedis.close();
    }
}

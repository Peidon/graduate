package edu.nju.filmreview.service;

import com.google.common.collect.Lists;
import edu.nju.filmreview.common.page.PageParams;
import edu.nju.filmreview.common.utils.CacheUtil;
import edu.nju.filmreview.model.Film;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

// redis-server path /usr/local/src/redis-5.0.3/src
@Service
public class RecommendService {

    private static final String HOT_FILM_KEY = "hot_film";

    private static final Logger logger = LoggerFactory.getLogger(RecommendService.class);

    @Autowired
    private FilmService filmService;

    /**
     * 点击触发热门打分
     * @param id
     */
    public void increase(Integer id) {
        try {
            Jedis jedis = CacheUtil.jPool.getResource();
            jedis.zincrby(HOT_FILM_KEY, 1.0D, id + "");
            jedis.zremrangeByRank(HOT_FILM_KEY, 0, -11);
            jedis.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }

    /**
     * 热门推荐
     * @param size
     * @return
     */
    public List<Film> getHotFilms(Integer size) {
        try {
            Jedis jedis = CacheUtil.jPool.getResource();
            jedis.zremrangeByRank(HOT_FILM_KEY, 0, -1 - size);
            Set<String> idSet = jedis.zrevrange(HOT_FILM_KEY, 0, -1);
            jedis.close();
            List<Long> ids = idSet.stream().map(Long::parseLong).collect(Collectors.toList());
            List<Film> fSet = query(ids,size);
            Film[] array = new Film[fSet.size()];
            for(Film film : fSet){
                final int idx = ids.indexOf(film.getId().longValue());
                array[idx] = film;
            }
            return Arrays.asList(array);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Lists.newArrayList();
        }
    }

    /**
     * 个性化推荐
     * @param size 页面大小
     * @param id 用户ID
     */
    public List<Film> recommend(Integer size, Long id) {
        Jedis jedis = CacheUtil.jPool.getResource();
        List<String> list = jedis.lrange(id.toString(), 0, -1);
        List<Long> ids = list.stream().map(Long::parseLong).collect(Collectors.toList());
        jedis.close();
        return query(ids,size);
    }

    public List<Film> query(List<Long> list,Integer size){
        list = list.subList(0, Math.min(list.size(), size));
        if (list.isEmpty()) {
            return Lists.newArrayList();
        }
        Film query = new Film();
        query.setIds(list);
        return filmService.queryList(query, PageParams.build(size, 1));
    }
}

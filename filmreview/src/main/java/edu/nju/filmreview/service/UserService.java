package edu.nju.filmreview.service;


import edu.nju.filmreview.common.utils.BeanHelper;
import edu.nju.filmreview.common.utils.HashUtils;
import edu.nju.filmreview.mapper.UserMapper;
import edu.nju.filmreview.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    public List<User> getUsers(){
        return userMapper.selectUsers();
    }
    @Transactional(rollbackFor = Exception.class)
    public boolean addAccount(User account) {
        account.setPasswd(HashUtils.encryPassword(account.getPasswd()));
        BeanHelper.setDefaultProp(account, User.class);
        BeanHelper.onInsert(account);
        userMapper.insert(account);
        return true;
    }

    /**
     * 用户名密码验证
     *
     * @param username
     * @param password
     * @return
     */
    public User auth(String username, String password) {
        User user = new User();
        user.setEmail(username);
        user.setPasswd(HashUtils.encryPassword(password));
        List<User> list = getUserByQuery(user);
        if (!list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    public List<User> getUserByQuery(User user) {
        List<User> list = userMapper.selectUsersByQuery(user);
        return list;
    }

    public void updateUser(User updateUser, String email) {
        updateUser.setEmail(email);
        BeanHelper.onUpdate(updateUser);
        userMapper.update(updateUser);
    }


    public User getUserById(Long id) {
        User queryUser = new User();
        queryUser.setId(id);
        List<User> users = getUserByQuery(queryUser);
        if (!users.isEmpty()) {
            return users.get(0);
        }
        return null;
    }

//    public void resetNotify(String username) {
//        mailService.resetNotify(username);
//    }

    /**
     * 重置密码操作
     *
     */
//    @Transactional(rollbackFor=Exception.class)
//    public User reset(String key,String password){
//        String email = getResetEmail(key);
//        User updateUser = new User();
//        updateUser.setEmail(email);
//        updateUser.setPasswd(HashUtils.encryPassword(password));
//        userMapper.update(updateUser);
//        mailService.invalidateRestKey(key);
//        return getUserByEmail(email);
//    }



//    public User getUserByEmail(String email) {
//        User queryUser = new User();
//        queryUser.setEmail(email);
//        List<User> users = getUserByQuery(queryUser);
//        if (!users.isEmpty()) {
//            return users.get(0);
//        }
//        return null;
//    }

//    public String getResetEmail(String key) {
//        String email = "";
//        try {
//            email =  mailService.getResetEmail(key);
//        } catch (Exception ignore) {
//        }
//        return email;
//    }
}

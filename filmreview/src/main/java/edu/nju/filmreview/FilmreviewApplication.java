package edu.nju.filmreview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilmreviewApplication {

    public static void main(String[] args) {
        SpringApplication.run(FilmreviewApplication.class, args);
    }
}

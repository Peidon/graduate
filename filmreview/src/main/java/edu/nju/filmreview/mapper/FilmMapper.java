package edu.nju.filmreview.mapper;

import edu.nju.filmreview.common.page.PageParams;
import edu.nju.filmreview.model.Film;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FilmMapper {
    List<Film> list(@Param("query") Film query, PageParams pageParams);

    Long selectPageCount(@Param("query") Film query);

    Film getOne(@Param("id")Integer id);

    void updateRate(@Param("rating") Double rating, @Param("id") Integer id);
}

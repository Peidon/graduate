package edu.nju.filmreview.mapper;

import java.util.List;

import edu.nju.filmreview.common.page.PageParams;
import edu.nju.filmreview.model.Comment;
import edu.nju.filmreview.model.Film;
import edu.nju.filmreview.model.UserMsg;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface CommentMapper {

  List<Film> selectFilm(@Param("film") Film query, @Param("pageParams") PageParams pageParams);

  Long selectFilmCount(@Param("film") Film query);

  int insertUserMsg(UserMsg userMsg);

  int updateFilm(Film film);

  int insert(Comment comment);

  List<Comment> selectComments(@Param("filmId") long filmId, @Param("size") int size);

  List<Comment> selectBlogComments(@Param("blogId") long blogId, @Param("size") int size);
  
}


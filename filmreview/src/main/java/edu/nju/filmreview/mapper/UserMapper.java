package edu.nju.filmreview.mapper;

import edu.nju.filmreview.model.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {
    List<User> selectUsers();

    void insert(User account);

    List<User> selectUsersByQuery(User user);

    void update(User updateUser);
}

package edu.nju.filmreview.mapper;

import java.util.List;

import edu.nju.filmreview.common.page.PageParams;
import edu.nju.filmreview.model.Blog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface BlogMapper {

  public List<Blog> selectBlog(@Param("blog") Blog query, @Param("pageParams") PageParams params);

  public Long selectBlogCount(Blog query);

}

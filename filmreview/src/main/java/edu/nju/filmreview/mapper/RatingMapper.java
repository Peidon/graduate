package edu.nju.filmreview.mapper;

import edu.nju.filmreview.model.Rating;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface RatingMapper {
    Rating getRating(@Param("uid") Long userId, @Param("fid") Integer filmId);

    void update(@Param("uid") Long uid, @Param("fid") Integer id, @Param("rating") Double rating);

    void insert(@Param("uid") Long uid, @Param("fid") Integer id, @Param("rating") Double rating);
}

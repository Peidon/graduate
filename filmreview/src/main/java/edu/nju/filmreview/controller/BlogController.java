package edu.nju.filmreview.controller;

import edu.nju.filmreview.common.page.PageData;
import edu.nju.filmreview.common.page.PageParams;
import edu.nju.filmreview.model.Blog;
import edu.nju.filmreview.model.Comment;
import edu.nju.filmreview.service.BlogService;
import edu.nju.filmreview.service.CommentService;
import edu.nju.filmreview.service.RecommendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class BlogController {
  
  @Autowired
  private BlogService blogService;
  
  @Autowired
  private CommentService commentService;
  
  @Autowired
  private RecommendService recommendService;
  
  @RequestMapping(value="blog/list",method={RequestMethod.POST,RequestMethod.GET})
  public String list(Integer pageSize, Integer pageNum, Blog query, ModelMap modelMap){
    PageData<Blog> ps = blogService.queryBlog(query, PageParams.build(pageSize, pageNum));
//    List<Film> films =  recommendService.getHotHouse(CommonConstants.RECOM_SIZE);
//    modelMap.put("recomFilms", films);
    modelMap.put("ps", ps);
    return "/blog/listing";
  }
  
  @RequestMapping(value="blog/detail",method={RequestMethod.POST,RequestMethod.GET})
  public String blogDetail(int id,ModelMap modelMap){
    Blog blog = blogService.queryOneBlog(id);
    List<Comment> comments = commentService.getBlogComments(id,8);
//    List<Film> houses =  recommendService.getBlogComments(CommonConstants.RECOM_SIZE);
//    modelMap.put("recomFilms", films);
    modelMap.put("blog", blog);
    modelMap.put("commentList", comments);
    return "/blog/detail";
  }
}

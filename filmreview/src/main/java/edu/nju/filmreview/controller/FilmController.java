package edu.nju.filmreview.controller;

import edu.nju.filmreview.common.page.PageData;
import edu.nju.filmreview.common.page.PageParams;
import edu.nju.filmreview.common.result.ResultMsg;
import edu.nju.filmreview.model.Comment;
import edu.nju.filmreview.model.Film;
import edu.nju.filmreview.service.FilmService;
import edu.nju.filmreview.service.RecommendService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class FilmController {

    @Autowired
    private FilmService filmService;

    @Autowired
    private RecommendService recommendService;

    @RequestMapping("/list")
    public String getList(ModelMap modelMap,@Param("pageNum") Integer pageNum){
        Film query = new Film();
        List<Film> films = recommendService.getHotFilms(5);
        PageData<Film> pageDate = filmService.queryFilm(query,PageParams.build(5, pageNum));
        modelMap.put("pageData", pageDate);
        modelMap.put("recomFilm",films);
        return "/film/listing";
    }

    @RequestMapping("/detail")
    public String filmDetail(ModelMap modelMap,@Param("id") String id){
        id = id.replace(",","");
        recommendService.increase(Integer.parseInt(id));
        Film film = filmService.detailFilm(Integer.parseInt(id));
        modelMap.put("film",film);
        modelMap.put("commentList", new ArrayList<Comment>());
        return "/film/detail";
    }

    @ResponseBody
    @RequestMapping("/rating")
    public ResultMsg rating(@Param("rating") Double rating, @Param("id") String id){
        id = id.replace(",","");
        filmService.updateRating(rating,Integer.parseInt(id));
        return ResultMsg.successMsg("ok");
    }
}

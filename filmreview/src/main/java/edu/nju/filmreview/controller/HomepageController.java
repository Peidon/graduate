package edu.nju.filmreview.controller;

import edu.nju.filmreview.common.constants.CommonConstants;
import edu.nju.filmreview.interceptor.UserContext;
import edu.nju.filmreview.model.Film;
import edu.nju.filmreview.model.User;
import edu.nju.filmreview.service.RecommendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.ArrayList;
import java.util.List;

@Controller
public class HomepageController {
  
  @Autowired
  private RecommendService recommendService;
  
  @RequestMapping("index")
  public String homePage(ModelMap modelMap){
    List<Film> films =  recommendService.getHotFilms(CommonConstants.RECOM_SIZE);
    modelMap.put("recomFilm", films);
    return "/homepage/index";
  }

  @RequestMapping("personal")
  public String personalRecommend(ModelMap modelMap){
    User user = UserContext.getUser();
    List<Film> films =  recommendService.recommend(CommonConstants.RECOM_SIZE,user.getId());
    films = films == null ? new ArrayList<Film>() : films;
    modelMap.put("recomFilm", films);
    return "/film/personal";
  }

  @RequestMapping("")
  public String index(ModelMap modelMap){
    return "redirect:/index";
  }
}

package edu.nju.filmreview.model;

import java.util.Date;

public class User {
    private Long id;

    private String phone;

    private String name;

    private String passwd;

    private String email;

    public User() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public void setConfirmPasswd(String confirmPasswd) {
        this.confirmPasswd = confirmPasswd;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    private String confirmPasswd;

    public Long getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }

    public String getPasswd() {
        return passwd;
    }

    public String getConfirmPasswd() {
        return confirmPasswd;
    }

    public Date getCreateTime() {
        return createTime;
    }

    private Date createTime;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

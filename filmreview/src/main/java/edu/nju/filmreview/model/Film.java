package edu.nju.filmreview.model;


import java.sql.Date;
import java.util.List;

public class Film {

    private Integer id;
    private String name;
    private Double rating;
    private Integer roundRating = 0;
    private String protagonism;//主演
    private String direct;//导演
    private String image;//电影海报
    private String theme;//主题

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    private Date releaseTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getRoundRating() {
        return roundRating;
    }

    public void setRoundRating(Integer roundRating) {
        this.roundRating = roundRating;
    }

    public String getProtagonism() {
        return protagonism;
    }

    public void setProtagonism(String protagonism) {
        this.protagonism = protagonism;
    }

    public String getDirect() {
        return direct;
    }

    public void setDirect(String direct) {
        this.direct = direct;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private List<Long> ids;

    public void setIds(List<Long> list) {
        this.ids = list;
    }

    public List<Long> getIds(){return ids;}

}

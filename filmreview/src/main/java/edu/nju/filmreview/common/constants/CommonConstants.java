package edu.nju.filmreview.common.constants;

public class CommonConstants {
	
	  public static final String USER_ATTRIBUTE = "loginUser";
	  
	  public static final String PLAIN_USER_ATTRIBUTE = "static/templates/user";
	  
	  public static final Integer RECOM_SIZE = 5;
	  
	  public static final Integer COMMENT_BLOG_TYPE  = 2;

}

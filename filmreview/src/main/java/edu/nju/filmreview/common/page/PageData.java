package edu.nju.filmreview.common.page;

import java.util.List;


public class PageData<T> {

	private List<T> list;
	private Pagination pagination;//pageNum,pageSize,page list
	
	public PageData(Pagination pagination,List<T> list){
		this.pagination = pagination;
		this.list = list;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}

	/**
	 * @param list 页面元素列表
	 * @param count 查询出的元素总数
	 * @param pageSize 页面元素个数
	 * @param pageNum 页面号码
	 */
	public static  <T> PageData<T> buildPage(List<T> list,Long count,Integer pageSize,Integer pageNum){
		Pagination _pagination = new Pagination(pageSize, pageNum, count);
		return new PageData<>(_pagination, list);
	}
	
}

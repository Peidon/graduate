package edu.nju.filmreview.common.page;

import java.util.List;

import com.google.common.collect.Lists;

public class Pagination {
	private int pageNum;
	private int pageSize;
	private long totalCount;
	private List<Integer> pages = Lists.newArrayList();

	public Pagination(Integer pageSize,Integer pageNum,Long totalCount) {
	   this.totalCount = totalCount;
	   this.pageNum = pageNum;
	   this.pageSize = pageSize;
	   for(int i = (pageNum - 2 > 0)?pageNum - 2:1;i<=pageNum;i++){
			pages.add(i);
	   }
	   Long pageCount = totalCount/pageSize + ((totalCount % pageSize == 0 ) ? 0: 1);
	   if (pageCount > pageNum + 2) {
		  for(int i= pageNum + 1; i<= pageNum + 2 ;i ++){
			  pages.add(i);
		  }
	   }
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	public List<Integer> getPages() {
		return pages;
	}

	public void setPages(List<Integer> pages) {
		this.pages = pages;
	}
}

<!DOCTYPE html>

<html lang="en-US">
<@common.header/>

<body class="page-homepage navigation-fixed-top page-slider page-slider-search-box" id="page-top" data-spy="scroll" data-target=".navigation" data-offset="90">
<!-- Wrapper -->
<div class="wrapper">

    <@common.nav/>

<!-- Page Content -->
<div id="page-content">
    <aside id="advertising" class="block">

    </aside>
    <section id="new-properties" class="block">
        <div class="container">
            <header class="section-title">
                <h2>推荐电影</h2>
                <a href="/film/list" class="link-arrow">更多</a>
            </header>
            <div class="row">
                <div class="col-md-9 col-sm-9">
                    <div class="property">
                        <section id="properties" class="display-lines">
                        <#list recomFilm as film>
                            <div class="property">
                            <figure class="tag status"></figure>
                            <div class="property-image">
                            <figure class="ribbon">${film.id}</figure>
                        <a href="/detail?id=${film.id}">
                            <img alt="" src="" style="width: 260px;height: 195px" >
                        </a>
                            </div>
                            <div class="info">
                            <header>
                        <a href="/detail?id=${film.id}"><h3>${film.name}</h3></a>
                            <figure>${film.theme}</figure>

                            </header>
                            <div class="tag price">${film.releaseTime?string('yyyy')}</div>
                            <aside>
                                <p>
                                </p>

                                <dl>

                                </dl>
                            </aside>
                            <a href="/detail?id=${film.id}" class="link-arrow">Read More</a>
                            </div>
                            </div>
                        </#list>
                        </section>
                    </div><!-- /.property -->
                </div><!-- /.col-md-3 -->
            </div><!-- /.row-->
        </div><!-- /.container-->
    </section><!-- /#new-properties-->
</div>
<!-- end Page Content -->
<!-- Page Footer -->
<@common.footer/>
    <!-- end Page Footer -->
    </div>

    <div id="overlay"></div>

<@common.js/>

<script>
    $(window).load(function(){
        initializeOwl(false);
    });
    $(document).ready(function() {
          var errorMsg   = "${errorMsg!""}";
          var successMsg = "${successMsg!""}";
          if(errorMsg){ 
              errormsg("error",errorMsg);
          }
          if(successMsg) {
              successmsg("success",successMsg);
          }
        })
</script>
</body>
</html>
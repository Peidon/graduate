<!DOCTYPE html>

<html lang="en-US">
<@common.header/>

<body class="page-sub-page page-listing-lines page-search-results" id="page-top">
<!-- Wrapper -->
<div class="wrapper">
    <!-- Navigation -->
   <@common.nav/><!-- /.navigation -->
    <!-- end Navigation -->
    <!-- Page Content -->
    <div id="page-content">
        <!-- Breadcrumb -->
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">电影列表</li>
            </ol>
        </div>
        <!-- end Breadcrumb -->
        <div class="container">
            <div class="row">
                <!-- Results -->
                <div class="col-md-9 col-sm-9">
                    <section id="results">
                        <header><h1>电影列表</h1></header>
                        <section id="search-filter">
                            <figure><h3><i class="fa fa-search"></i>搜索结果:</h3>
                                <span class="search-count"></span>
                                 <div class="sorting">
                                    <div class="form-group">
                                        <select name="sorting" id="sorting">
                                        <option value="">上映时间排序</option>
                                            <option value="time_asc" >升序</option>
                                            <option value="time_desc" >降序</option>
                                        </select>
                                    </div><!-- /.form-group -->
                                </div>
                            </figure>
                        </section>
                    <section id="properties" class="display-lines">
                      <#list pageData.list as film>
                       
                            <div class="property">
                                <figure class="tag status"></figure>
                                <div class="property-image">
                                    <figure class="ribbon">${film.id}</figure>
                                    <a href="/detail?id=${film.id}">
                                        <img alt="" src="" style="width: 260px;height: 195px" >
                                    </a>
                                </div>

                                <div class="info">
                                    <header>
                                        <a href="/detail?id=${film.id}"><h3>${film.name}</h3></a>
                                        <figure>${film.theme}</figure>

                                    </header>
                                    <div class="tag price">${film.releaseTime?string('yyyy')}</div>
                                    <aside>
                                         <p>
                                        </p>

                                        <dl>

                                        </dl>
                                    </aside>
                                    <a href="/detail?id=${film.id}" class="link-arrow">Read More</a>
                                </div>
                            </div>
                        </#list>

                       </section>
                            <!-- Pagination -->
                            <div class="center">
                                 <@common.paging pageData.pagination/>
                            </div><!-- /.center-->
                        </section><!-- /#properties-->
                    </section><!-- /#results -->
                </div><!-- /.col-md-9 -->
                <!-- end Results -->

                <!-- sidebar -->
                <div class="col-md-3 col-sm-3">
                    <section id="sidebar">
                        <aside id="edit-search">
                            <header><h3>Search Properties</h3></header>
                            <form role="form" id="searchForm" class="form-search" method="post" action="/list">

                                <div class="form-group">
                                    <input type="text" class="form-control" id="search-box-property-id" value="" name="name" >
                                </div>
                                <div class="form-group">
                                    <select name="type">
                                        <option value="1" selected>选择</option>
                                        <option value="1">电影名</option>
                                        <option value="2">类型</option>
                                    </select>
                                </div><!-- /.form-group -->
                                <input type="text" value="" name=sort hidden="true">
                               
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default">搜索</button>
                                </div><!-- /.form-group -->
                            </form><!-- /#form-map -->
                        </aside><!-- /#edit-search -->
                        <aside id="featured-properties">
                            <header><h3>热门电影</h3></header>
                            <#list recomFilm as film>
                                <div class="property small">
                                <a href="/film/detail?id=${film.id}">
                                    <div class="property-image">
                                        <img alt="" src="" style="width: 100px;height: 75px">
                                    </div>
                                </a>
                                <div class="info">
                            <a href="/detail?id=${film.id}"><h4>${film.name}</h4></a>
                                <figure>${film.theme} </figure>
                                <div class="tag price">上映年份：${film.releaseTime?string('yyyy')}</div>
                                </div>
                                </div>
                            </#list>
                            
                        </aside><!-- /#featured-properties -->
                        
                    </section><!-- /#sidebar -->
                </div><!-- /.col-md-3 -->
                <!-- end Sidebar -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>
    <!-- end Page Content -->
    <!-- Page Footer -->
     <@common.footer/>
    <!-- end Page Footer -->
</div>

<@common.js/>
<!--[if gt IE 8]>
<script type="text/javascript" src="assets/js/ie.js"></script>
<![endif]-->
 <script  type="text/javascript" >

 function nextPage(pageNum, pageSize) {

    if ($('#searchForm').length != 0) {
        var url = $('#searchForm').attr('action');
        var data = "?pageNum=" + pageNum + "&pageSize=" + pageSize + "&" + $('#searchForm').serialize();
        window.location.href = url + data;
    } else {
        var url = window.location.href;
           url  = url.split('?')[0];
        var data = "?pageNum=" + pageNum + "&pageSize=" + pageSize ;
        window.location.href = url + data;
    }
}
     

     $(document).ready(function() {
          var errorMsg   = "${errorMsg!""}";
          var successMsg = "${successMsg!""}";
          if(errorMsg){ 
              errormsg("error",errorMsg);
          }
          if(successMsg) {
              successmsg("success",successMsg);
          }
        })
      
      
  
     
      $('#sorting').change(function() {
           var type =  $(this).val();
           if (!type) {
               return;
           }
           window.location.href=  "/house/list?sort="+type+"&name=" + "${(vo.name)!}" + "&type=" + "${(vo.type)!0}" ;
       });

        
 </script>

</body>
</html>
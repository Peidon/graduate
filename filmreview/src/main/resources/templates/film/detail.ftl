<!DOCTYPE html>

<html lang="en-US">
<@common.header/>


<body class="page-sub-page page-property-detail" id="page-top">
<!-- Wrapper -->
<div class="wrapper">
    <!-- Navigation -->
    <@common.nav/><!-- /.navigation -->
    <!-- end Navigation -->
    <!-- Page Content -->
    <div id="page-content">
        <!-- Breadcrumb -->
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Film Detail</li>
            </ol>
        </div>
        <!-- end Breadcrumb -->

        <div class="container">
            <div class="row">
                <!-- Property Detail Content -->
                <div class="col-md-9 col-sm-9">
                    <section id="property-detail">
                        <header class="property-title">
                            <h1>${film.name}</h1>
                            <figure>${film.releaseTime?string('yyyy')}</figure>

                            <#if loginUser??>
                                <span class="actions">
                                <!--<a href="#" class="fa fa-print"></a>-->
                                <a href="#" class="bookmark" data-bookmark-state="empty"

                                ><span class="title-add">Add to bookmark</span><span
                                            class="title-added">Added</span></a>
                            </span>
                            </#if>
                        </header>
                        <section id="property-gallery">
                            <div class="owl-carousel property-carousel">
                                <div class="property-slide">
                                    <a href="static/imgs/film.jpg" class="image-popup">
                                        <div class="overlay"><h3>Front View</h3></div>
                                        <img alt="" src="static/imgs/film.jpg">
                                    </a>
                                </div><!-- /.property-slide -->
                                <div class="property-slide">
                                    <a href="static/imgs/film.jpg" class="image-popup">
                                        <div class="overlay"><h3>Front View</h3></div>
                                        <img alt="" src="static/imgs/film.jpg">
                                    </a>
                                </div>
                            </div><!-- /.property-carousel -->
                        </section>
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <section id="quick-summary" class="clearfix">
                                    <header><h2>详情</h2></header>
                                    <dl>
                                        <dt>导演</dt>
                                        <dd><#if film.direct??>${film.direct}<#else>未知</#if></dd>
                                        <dt>主演</dt>
                                        <dd>
                                            <span class="tag price"><#if film.protagonism??>${film.protagonism}<#else>未知</#if></span>
                                        </dd>
                                        <dt>类型:</dt>
                                        <dd>${film.theme}</dd>
                                        <dt>上映年份:</dt>
                                        <dd>${film.releaseTime?string('yyyy')}</dd>
                                        <dt>评分:</dt>
                                        <dd>
                                            <div class="rating rating-overall"
                                                 data-score="<#if film.rating??>${film.rating}<#else>0</#if>"></div>
                                        </dd>
                                    </dl>
                                </section><!-- /#quick-summary -->
                            </div><!-- /.col-md-4 -->
                            <div class="col-md-8 col-sm-12">
                                <section id="description">
                                    <header><h2>描述</h2></header>
                                    故事发生在遥远的未来，地球流浪到了冥王星...
                                </section><!-- /#description -->

                                <header>您的评价</header>
                                <div class="rating rating-user">
                                    <div id = "inn" class="inner"></div>
                                    <div id = "hint" class="target-hint"></div>
                                </div>

                            <hr class="thick">
                            <section id="comments">
                                <div class="agent-form">
                                    <form role="form" id="form-contact-agent" method="post" action="#" class="clearfix">
                                        <input type="hidden" name="filmId" value="${film.id}">
                                        <div class="form-group">
                                            <label for="form-contact-agent-message">评论</label>
                                            <textarea class="form-control" id="form-contact-agent-message" rows="2"
                                                      name="content" required></textarea>
                                        </div><!-- /.form-group -->
                                        <div class="form-group">
                                            <button type="submit" class="btn pull-right btn-default"
                                                    id="form-contact-agent-submit">评论
                                            </button>
                                        </div><!-- /.form-group -->
                                        <div id="form-contact-agent-status"></div>
                                    </form><!-- /#form-contact -->
                                </div>
                                <header><h2 class="no-border">Comments</h2></header>
                                <ul class="comments">
                                    <#list commentList as comment>
                                        <li class="comment" style="width: 830px;">
                                        <figure>
                                        <div class="image">
                                        <img alt="" src="${comment.avatar}">
                                        </div>
                                        </figure>
                                        <div class="comment-wrapper">
                                        <div class="name pull-left">${comment.userName}</div>
                                        <span class="date pull-right"><span
                                            class="fa fa-calendar"></span>${(comment.createTime)?datetime}</span>
                                        <p>${comment.content}
                                        </p>
                                        <hr>
                                        </div>
                                        </li>
                                    </#list>
                                </ul>
                            </section>
                        </div><!-- /.col-md-12 -->

                </div><!-- /.row -->
                </section><!-- /#property-detail -->
            </div><!-- /.col-md-9 -->
            <!-- end Property Detail Content -->

        </div><!-- /.row -->
    </div><!-- /.container -->
</div>
<!-- end Page Content -->
<!-- Page Footer -->
<@common.footer/>
    <!-- end Page Footer -->
    </div>

<@common.js/>

<script type="text/javascript">
    $(window).load(function(){
        initializeOwl(false);
    });

     $(document).ready(function() {
          $('#inn').raty({
                    number: 5,
                    path: '/static/assets/img',
                    starOff : 'big-star-off.png',
                    starOn  : 'big-star-on.png',
                    width: 150,
                    target: '#hint',
                    targetType : 'number',
                    targetKeep: true,
                    click: function(score, evt) {
                        showRatingForm();
                        $.ajax({
                               url: "/rating?id=${film.id}&rating="+score,
                               type: 'GET',
                               cache:false,
                               timeout:60000
                              })
                              .done(function(ret) {

                              })
                    }
                });

          });

    var bookmarkButton = $(".bookmark");

    
    bookmarkButton.on("click", function() {
        if (bookmarkButton.data('bookmark-state') == 'empty') {
            commonAjax('/bookmark?id=${film.id}');
        } else if (bookmarkButton.data('bookmark-state') == 'added') {
            commonAjax('/unbookmark?id=${film.id}');
        }
    });
    
   

        

</script>

</body>
</html>